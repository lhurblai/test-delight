# Test-delight

**Test technique pour un stage de fin d'études dans la société Delight Data**


1. Bibliotheque utilisées : 
- http.client
- json
- Pandas
- Matplotlib


2. Instruction d'utilisation :
- Telecharger le fichier "test.py" dans un dossier de votre choix
- Dans un terminal, lancez l'instruction "./test.py"
- Il y aura 2 à 3 minutes d'attente le temps du traitement des données.


3. Mise à disposition des données
- Un export des données ainsi que des graphes sera disponible à la fin.
- Un export des données ainsi que des graphes sont également disposition dans le git en cas de problème avec le script
