#!/usr/bin/env python3
# coding: utf-8

import http.client, json, pandas as pd
import matplotlib.pyplot as plt 

payload= "{}"
info_top500=[]
data_top500=[]
info_user=[]

# Etablissement de la connexion vers le site "hacker-news.firebaseio.com"
conn = http.client.HTTPSConnection("hacker-news.firebaseio.com")

# Récupération des numéros des stories du top500 du moment
conn.request("GET", "/v0/topstories.json", payload)
res = conn.getresponse()
data_top500 = json.loads(res.read())

# Récupération des informations générales sur chaque story, issu du top500
for item in data_top500:
    conn.request("GET", "/v0/item/"+str(item)+".json", payload)
    res = conn.getresponse()
    value = json.loads(res.read())
    info_top500.append(value)

# Fermeture de la connexion
conn.close()

# Convertir les informations en dataframe pandas
dataframe = pd.DataFrame(info_top500)

# Récupération seulement de la colonne "by"
# "by" nous donne l'id des utilisateur ayant postés les différentes stories
dataframe_by = dataframe["by"]

# Suppression des doublons, dans le cas où un contributeur a posté plusieurs stories dans le top500
# Et en écrassant le dataframe
if len(dataframe_by.unique()) < len(dataframe_by.index):
        dataframe_by.drop_duplicates(inplace=True)

# Réindaxage pour ne plus avoir les lignes vides + suppression de la colonne qui s'ajoute avec l'opération
dataframe_by = dataframe_by.reset_index()
del dataframe_by['index']

# Récupération des doonées des contributeurs
conn = http.client.HTTPSConnection("hacker-news.firebaseio.com")
for user in dataframe_by["by"]:
    conn.request("GET", "/v0/user/"+str(user)+".json", payload)
    res = conn.getresponse()
    value = json.loads(res.read())
    info_user.append(value)
conn.close()

# Convertir les informations en dataframe pandas
dataframe_user = pd.DataFrame(info_user)

# # Réindexation des colonnes afin d'avoir un ordre plus logique
# dataframe_user=dataframe_user[['id','created','karma','submitted','about']]

# Affichage de chaque contributeur avec leurs informations respectives en ajouter les mauvais karmas
for i in range(len(dataframe_user)):
    print("-- "+str(i+1)+" --")
    print("Contributeur : " +dataframe_user["id"][i]+ "  |  Date de création : " +str(dataframe_user["created"][i])+ "  |  Karma de " +str(dataframe_user["karma"][i]))
    print("     Nombre de publication : " +str(len(dataframe_user["submitted"][i])))
    print("     Autres : " +str(dataframe_user["about"][i]))
    print("")

print("")
print(" Un export a été réalisé en cas de besoin : export_user_top500.csv")
print("")
dataframe_user.to_csv(r'export_user_top500.csv', index = True, header=True)

# J'ai délibérément évité d'afficher la totalité des publications pour chaque personne.
# J'ai remarqué très rapidement que l'affichage était pollué et donc difficile à lire.
# A titre indicatif, j'affiche le nombre de publication qui me semble raisonnable.



# Vérification si des karmas sont négatifs dans les contributeurs du top500
info_bad_karma = pd.DataFrame(info_user)
for i in range(len(dataframe_user)):
    if dataframe_user["karma"][i] > 0 :   
        info_bad_karma.drop(i,inplace=True)  # Suppression des karmas positifs pour réaliser un traitement que sur les négatifs

info_bad_karma = info_bad_karma.reset_index()
del info_bad_karma['index']

# Affichage des karmas négatifs s'il y en a !
if not info_bad_karma.empty :
    print("----- ATTENTION -----")
    print(" Il y a des contributeurs avec un karma négatif, en voici la liste :")
    for i in range(len(info_bad_karma)):
        print("-- "+str(i+1)+" -- Contributeur : " +info_bad_karma["id"][i]+"  avec un Karma de " +str(info_bad_karma["karma"][i]))
        print("")
else:
    print(" Pas de BAD KARMA")
    print("")

# Transformation de la liste des "submitted" en un nombre
nbr_line=0
for sub in dataframe_user["submitted"]:
    dataframe_user.loc[nbr_line,"submitted"] = len(sub)
    nbr_line+=1

# Transformation du timestamp en datatime
dataframe_user["created"] = pd.to_datetime(dataframe_user["created"],unit='s')
df_time = dataframe_user.sort_values(by=['created'])


# Changement des paramètres par défaut pour l'affichage différents graphes
plt.rcParams["figure.figsize"] = (20, 8)

# affichage d'un graphique plot + extraction
plt.plot(df_time["created"].dt.date, df_time["submitted"])
plt.xlabel("Account creation date")
plt.ylabel("Number of submitted")
plt.savefig('created_submitted_plot.png')
plt.clf()

# affichage d'un graphique plot + extraction
plt.bar(df_time["created"].dt.year, df_time["submitted"])
plt.xlabel("Account creation date")
plt.ylabel("Number of submitted")
plt.savefig('created_submitted_bar.png')
plt.clf()

plt.scatter(df_time["created"].dt.year, df_time["submitted"], color="blue")
plt.xlabel("Account creation date")
plt.ylabel("Number of submitted")
plt.savefig('created_submitted_scatter.png')
plt.clf()

print(" Un export des graphes a été réalisé en cas de besoin : ")
print("      - created_submitted_plot.png, created_submitted_bar.png & created_submitted_scatter.png")
print("")
